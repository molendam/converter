package com.conversion.calculator.consoleApp;

public enum CommandStatus {
    OK,NIEPRAWIDŁOWY_FORMAT_POLECENIA,BRAK_TAKIEGO_TYPU_KONWERSJI, NIEOBSŁUGIWANA_WARTOŚĆ
}
