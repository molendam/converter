package com.conversion.calculator.consoleApp;

import com.conversion.calculator.converter.ImperialToMetricConverter;
import com.conversion.calculator.converter.MetricToImperialConverter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ConverterApp {

    private Scanner scanner = new Scanner(System.in);
    private String convertFrom = null;
    private String convertTo = null;
    private int scaleBigDecimal = 3; //dodać opcję do zmieniania wyświetlania wartości po przecinku

    public void run() {
        System.out.println("Witaj w konwerterze jednostek!\n ");
        mainMenu();
        while (true) {
            String userInput = scanner.nextLine().toLowerCase();
            switch (userInput) {
                case "help":
                    showHelp();
                    break;
                case "exit":
                    return;
                default:
                    startConversion(userInput);
                    break;
            }
        }
    }

    private void startConversion(String userInput) {
        CommandStatus cs = checkCommand(userInput);
        switch (cs) {
            case OK:
                try {
                    BigDecimal result = convert(userInput);
                    String description = convertFrom + ": " + splitUserInput(userInput)[1] + "\n"
                            + convertTo + ": " + result.setScale(scaleBigDecimal, RoundingMode.HALF_UP);
                    System.out.println(description);
                } catch (IllegalArgumentException iae) {
                    System.out.println(iae.getMessage());
                    mainMenu();
                }
                break;
            case NIEPRAWIDŁOWY_FORMAT_POLECENIA:
                System.out.println("Niepoprawny format polecenia");
                mainMenu();
                break;
            case BRAK_TAKIEGO_TYPU_KONWERSJI:
                System.out.println("Brak takiego typu konwersji");
                mainMenu();
                break;
            case NIEOBSŁUGIWANA_WARTOŚĆ:
                System.out.println("Nieobsługiwana wartość");
                mainMenu();
                break;
        }
    }

    private void mainMenu() {
        System.out.println(
                "Aby wyświetlić dostępne konwersje:              help\n" +
                        "Konwersja :                                 xx value\n" +
                        "Aby zakończyć:                                  exit");
    }

    private BigDecimal parse(String value) {
        return new BigDecimal(value.replace(",","."));
    }

    private String[] splitUserInput(String userInput) {
        return userInput.split("\\s+");
    }

    private BigDecimal convert(String userInput) {
        String[] splitUserInput = splitUserInput(userInput);
        String ui = splitUserInput[0];
        BigDecimal value = parse(splitUserInput[1]);
        switch (ui) {
            case "mf":
                setUnits("Metry", "Stopy");
                return MetricToImperialConverter.metersOnFeets(value);
            case "fm":
                setUnits("Stopy", "Metry");
                return ImperialToMetricConverter.feetsOnMeters(value);
            case "ci":
                setUnits("Centymetry", "Cale");
                return MetricToImperialConverter.centimetersOnInchs(value);
            case "ic":
                setUnits("Cale", "Centymetry");
                return ImperialToMetricConverter.inchsOnCentimeters(value);
            case "lg":
                setUnits("Litry", "Galony");
                return MetricToImperialConverter.litreOnGallon(value);
            case "gl":
                setUnits("Galony", "Litry");
                return ImperialToMetricConverter.gallonOnLitre(value);
            case "kp":
                setUnits("kilo", "funty");
                return MetricToImperialConverter.kilogramOnPound(value);
            case "pk":
                setUnits("funty", "kilo");
                return ImperialToMetricConverter.poundOnKilogram(value);
            case "cf":
                setUnits("Celsjusz", "Fahrenhait");
                return MetricToImperialConverter.celsiusOnFahrenheit(value);
            case "fc":
                setUnits("Fahrenhait", "Celsjusz");
                return ImperialToMetricConverter.fahrenheitOnCelsius(value);
        }
        return null;
    }

    private void setUnits(String convertFrom, String convertTo) {
        this.convertFrom = convertFrom;
        this.convertTo = convertTo;
    }


    private CommandStatus checkCommand(String userInput) {
        if (userInput.isEmpty()) {
            return CommandStatus.NIEPRAWIDŁOWY_FORMAT_POLECENIA;
        }
        Pattern p = Pattern.compile("[a-zA-Z]{2} +-?[0-9]+[[.|,]?[0-9]+]* *");
        if (!p.matcher(userInput).matches()) {
            return CommandStatus.NIEPRAWIDŁOWY_FORMAT_POLECENIA;
        }
        String command = splitUserInput(userInput)[0];
        if (!getCommandShortcuts().contains(command)) {
            return CommandStatus.BRAK_TAKIEGO_TYPU_KONWERSJI;
        }
        try {
            parse(splitUserInput(userInput)[1]);
        } catch (NumberFormatException ne) {
            return CommandStatus.NIEOBSŁUGIWANA_WARTOŚĆ;
        }

        return CommandStatus.OK;
    }

    private List<String> getCommandShortcuts() {
        List<String> shortcuts = new ArrayList<>();
        for (String descr : getCommandDescriptionWithShortcuts()
        ) {
            shortcuts.add(String.format(descr.substring(descr.length() - 8, descr.length() - 6)));
        }
        return shortcuts;
    }

    private void showHelp() {
        String fullOptions = "";
        for (String cs : getCommandDescriptionWithShortcuts()
        ) {
            fullOptions += cs + "\n";
        }
        System.out.println(fullOptions);
    }

    private List<String> getCommandDescriptionWithShortcuts() {
        List<String> commands = new ArrayList<>();
        commands.add("metry/stopy:          mf value");
        commands.add("stopy/metry:          fm value");
        commands.add("centymetry/cale:      ci value");
        commands.add("cale/centymetry:      ic value");
        commands.add("litry/galony:         lg value");
        commands.add("galony/litry:         gl value");
        commands.add("kilo/funty:           kp value");
        commands.add("funty/kilo:           pk value");
        commands.add("Celsjusz/Fahrenhait:  cf value");
        commands.add("Fahrenhait/Celsjusz:  fc value");
        return commands;
    }
}
