package com.conversion.calculator.converter;

final class ConvertValues {


    static float getConvertMetreOnFeet() {
        return 3.2808399f;
    }

    static float getConvertCentimetreOnInch() {
        return 0.393700787f;
    }

    static float getConvertLitreOnGallon() {
        return 0.219968813f;
    }

    static float getConvertKilogramOnPound() {
        return 2.20462262f;
    }

}
