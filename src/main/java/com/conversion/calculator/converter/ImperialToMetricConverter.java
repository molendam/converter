package com.conversion.calculator.converter;

//import org.apache.commons.lang3.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class ImperialToMetricConverter {

    private static int scale = 3;

    private static RoundingMode rm = RoundingMode.HALF_UP;

    public static int getScale() {
        return scale;
    }

    public static void setScale(int scale) {
        ImperialToMetricConverter.scale = scale;
    }

    public static RoundingMode getRm() {
        return rm;
    }

    public static void setRm(RoundingMode rm) {
        ImperialToMetricConverter.rm = rm;
    }

    public static BigDecimal feetsOnMeters(BigDecimal feets) {
        return
                imperialToMetric(ConvertValues.getConvertMetreOnFeet(), feets);
    }

    public static BigDecimal inchsOnCentimeters(BigDecimal inchs) {

        return imperialToMetric(ConvertValues.getConvertCentimetreOnInch(), inchs);
    }

    public static BigDecimal gallonOnLitre(BigDecimal gallons) {
        return imperialToMetric(ConvertValues.getConvertLitreOnGallon(), gallons);
    }

    public static BigDecimal poundOnKilogram(BigDecimal pounds) {
        return imperialToMetric(ConvertValues.getConvertKilogramOnPound(), pounds);
    }

    public static BigDecimal fahrenheitOnCelsius(BigDecimal fahrenheits) {
        if (fahrenheits==null) {
            throw new IllegalArgumentException("Value can't be null");
        }
        return (fahrenheits.add(new BigDecimal("-32"))).divide(new BigDecimal("1.8"), getScale(), getRm());
    }


    private static BigDecimal imperialToMetric(float unitValue, BigDecimal userValue) {
        if (userValue==null) {
            throw new IllegalArgumentException("Value can't be null");
        }
        if (userValue.signum() < 0) {
            throw new IllegalArgumentException("Values only greater than or equal to 0");
        }
        return userValue.divide(new BigDecimal(unitValue) , scale, rm);
    }


}
