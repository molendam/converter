package com.conversion.calculator.converter;

import com.conversion.calculator.consoleApp.ConverterApp;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
       ConverterApp cApp = new ConverterApp();
       cApp.run();
    }
}
