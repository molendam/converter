package com.conversion.calculator.converter;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class MetricToImperialConverter {

    private static int scale = 3;
    private static RoundingMode rm = RoundingMode.HALF_UP;

    public static int getScale() {
        return scale;
    }

    public static void setScale(int scale) {
        MetricToImperialConverter.scale = scale;
    }

    public static RoundingMode getRm() {
        return rm;
    }

    public static void setRm(RoundingMode rm) {
        MetricToImperialConverter.rm = rm;
    }

    public static BigDecimal metersOnFeets(BigDecimal meters) {
        return metricToImperial(meters, ConvertValues.getConvertMetreOnFeet());
    }

    public static BigDecimal centimetersOnInchs(BigDecimal centimeters) {
        return metricToImperial(centimeters, ConvertValues.getConvertCentimetreOnInch());
    }

    public static BigDecimal litreOnGallon(BigDecimal litre) {
        return metricToImperial(litre, ConvertValues.getConvertLitreOnGallon());
    }

    public static BigDecimal kilogramOnPound(BigDecimal kilogram) {
        return metricToImperial(kilogram, ConvertValues.getConvertKilogramOnPound());
    }

    public static BigDecimal celsiusOnFahrenheit(BigDecimal celsius) {
        if (celsius==null) {
            throw new IllegalArgumentException("Value can't be null");
        }
        return (celsius.multiply(new BigDecimal(1.8))).add(new BigDecimal("32")).setScale(scale, rm);//,scale,rm;
    }

    private static BigDecimal metricToImperial(BigDecimal userValue, float unitValue) {
        if ( userValue == null) {
            throw new IllegalArgumentException("Value can't be null");
        }

        if (userValue.signum() < 0) {
            throw new IllegalArgumentException("Values only greater than or equal to 0");
        }
        return userValue.multiply(new BigDecimal(unitValue)).setScale(scale, rm);
    }


}
