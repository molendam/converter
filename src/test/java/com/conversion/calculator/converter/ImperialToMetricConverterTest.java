package com.conversion.calculator.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

public class ImperialToMetricConverterTest {

    @BeforeAll
    static void setUp() {
        ImperialToMetricConverter.setRm(RoundingMode.HALF_UP);
        ImperialToMetricConverter.setScale(4);
    }

    //Should return correct values after conversion

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertFeetsOnMeters() {
        return Stream.of(Arguments.of("1", "0.3048"),
                Arguments.of("5", "1.5240"),
                Arguments.of("300.543", "91.6055"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"654553010.6118"));
    }

    @ParameterizedTest(name = "{index}:{0} feets = {1} meters ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertFeetsOnMeters")
    void shouldReturnCorrectMetersValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.feetsOnMeters(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertInchsOnCentimeters() {
        return Stream.of(Arguments.of("1", "2.5400"),
                Arguments.of("5", "12.7000"),
                Arguments.of("300.543", "763.3792"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"5454608586.9256"));

    }

    @ParameterizedTest(name = "{index}:{0} inchs = {1} centimeters ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertInchsOnCentimeters")
    void shouldReturnCorrectCentimetersValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.inchsOnCentimeters(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertGallonOnLitre() {
        return Stream.of(Arguments.of("1", "4.5461"),
                Arguments.of("5", "22.7305"),
                Arguments.of("300.543", "1366.2982"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"9762673355.3067"));
    }

    @ParameterizedTest(name = "{index}:{0} gallon = {1} litre ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertGallonOnLitre")
    void shouldReturnCorrectLitreValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.gallonOnLitre(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertPoundOnKilogram() {
        return Stream.of(Arguments.of("1", "0.4536"),
                Arguments.of("5", "2.2680"),
                Arguments.of("300.543", "136.3240"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"974082247.6812"));
    }

    @ParameterizedTest(name = "{index}:{0} pound = {1} kilogram ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertPoundOnKilogram")
    void shouldReturnCorrectKilogramValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.poundOnKilogram(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertFahrenheitOnCelsius() {
        return Stream.of(Arguments.of("1", "-17.2222"),
                Arguments.of("5", "-15.0000"),
                Arguments.of("300.543", "149.1906"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"1193046452.7778"));
    }

    @ParameterizedTest(name = "{index}:{0} fahrenheit = {1} celsious")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertFahrenheitOnCelsius")
    void shouldReturnCorrectCelsiusValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.fahrenheitOnCelsius(value);
        Assertions.assertEquals(expected, result);
    }

    //Should throw IllegalArgumentException when value is less than 0

    static Stream<Arguments> provideBigDecimalsLessthan0() {
        return Stream.of(Arguments.of("-1"),
                Arguments.of("-9999.9999"),
                Arguments.of("-999999999999999999999999999999999999"),
                Arguments.of(Integer.toString(Integer.MIN_VALUE) ));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForFeetsValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ImperialToMetricConverter.feetsOnMeters(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForInchsValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ImperialToMetricConverter.inchsOnCentimeters(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForGallonsValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ImperialToMetricConverter.gallonOnLitre(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForPoundsValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ImperialToMetricConverter.poundOnKilogram(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldDoesNotThrowForFahrenheitsValueLessThan0(BigDecimal value) {
        Assertions.assertDoesNotThrow(() -> ImperialToMetricConverter.fahrenheitOnCelsius(value));
    }
    static Stream<Arguments> provideValuesLessthan0() {
        return Stream.of(Arguments.of("-1","-18.3333"),
                Arguments.of("-9999.9999","-5573.3333"),
                Arguments.of("-999999999999999999999999999999999999","-555555555555555555555555555555555572.7778"),
                Arguments.of(Integer.toString(Integer.MIN_VALUE) , "-1193046488.8889"));
    }
    @ParameterizedTest
    @MethodSource("provideValuesLessthan0")
    void shouldReturnCorrectCelsiusValuesValueLessThan0(BigDecimal value, BigDecimal expected) {
        BigDecimal result = ImperialToMetricConverter.fahrenheitOnCelsius(value);
        Assertions.assertEquals(expected, result);
    }



    //Should throw IllegalArgumentException when value is null

    @Test
    void shouldThrowIllegalArgumentExceptionWhenValueEqualsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.feetsOnMeters(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.fahrenheitOnCelsius(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.gallonOnLitre(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.inchsOnCentimeters(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.poundOnKilogram(null));
    }
}
