package com.conversion.calculator.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

public class MetricToImperialConverterTest {
    @BeforeAll
    static void setUp() {
        MetricToImperialConverter.setRm(RoundingMode.HALF_UP);
        MetricToImperialConverter.setScale(4);
    }

    //Should return correct values after conversion
    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertMetersOnFeets() {
        return Stream.of(Arguments.of("1", "3.2808"),
                Arguments.of("5", "16.4042"),
                Arguments.of("300.543", "986.0335"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"7045550076.7192"));
    }

    @ParameterizedTest(name = "{index}:{0} meters = {1} feets")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertMetersOnFeets")
    void shouldReturnCorrectFeetsValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = MetricToImperialConverter.metersOnFeets(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertCentimetersOnInchs() {
        return Stream.of(Arguments.of("1", "0.3937"),
                Arguments.of("5", "1.9685"),
                Arguments.of("300.543", "118.3240"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"845465983.6063"));

    }

    @ParameterizedTest(name = "{index}:{0}  centimeters = {1} inchs ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertCentimetersOnInchs")
    void shouldReturnCorrectInchsValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = MetricToImperialConverter.centimetersOnInchs(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertLitreOnGallon() {
        return Stream.of(Arguments.of("1", "0.2200"),
                Arguments.of("5", "1.0998"),
                Arguments.of("300.543", "66.1101"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"472379423.7800"));
    }

    @ParameterizedTest(name = "{index}:{0} litre = {1}  gallon ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertLitreOnGallon")
    void shouldReturnCorrectGallonValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = MetricToImperialConverter.litreOnGallon(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertKilogramOnPound() {
        return Stream.of(Arguments.of("1", "2.2046"),
                Arguments.of("5", "11.0231"),
                Arguments.of("300.543", "662.5839"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"4734390781.7954"));
    }

    @ParameterizedTest(name = "{index}:{0} kilogram = {1}  pound ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertKilogramOnPound")
    void shouldReturnCorrectPoundValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = MetricToImperialConverter.kilogramOnPound(value);
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideBigDecimalsForCorrectValuesWhenConvertCelsiusOnFahrenheit() {
        return Stream.of(Arguments.of("1", "33.8000"),
                Arguments.of("5", "41.0000"),
                Arguments.of("300.543", "572.9774"),
                Arguments.of(Integer.toString(Integer.MAX_VALUE),"3865470596.6000"),
                        Arguments.of(Integer.toString(Integer.MIN_VALUE),"-3865470534.4000"));

    }

    @ParameterizedTest(name = "{index}:{0} Celsius = {1}  Fahrenheit ")
    @MethodSource("provideBigDecimalsForCorrectValuesWhenConvertCelsiusOnFahrenheit")
    void shouldReturnCorrectFahrenheitValues(BigDecimal value, BigDecimal expected) {
        BigDecimal result = MetricToImperialConverter.celsiusOnFahrenheit(value);
        Assertions.assertEquals(expected, result);
    }

    //Should throw IllegalArgumentException when value is less than 0

    static Stream<Arguments> provideBigDecimalsLessthan0() {
        return Stream.of(Arguments.of("-1"), Arguments.of("-9999.9999"),
                Arguments.of("-999999999999999999999999999999999999"),
                Arguments.of(Integer.toString(Integer.MIN_VALUE)));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForMetersValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.metersOnFeets(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForCentimetersValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.centimetersOnInchs(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForLitreValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.litreOnGallon(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldThrowIllegalArgumentExceptionForKilogramValueLessThan0(BigDecimal value) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.kilogramOnPound(value));
    }

    @ParameterizedTest
    @MethodSource("provideBigDecimalsLessthan0")
    void shouldNotThrowIllegalArgumentExceptionForCelsiusValueLessThan0(BigDecimal value) {
        Assertions.assertDoesNotThrow(() -> MetricToImperialConverter.celsiusOnFahrenheit(value));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenValueEqualsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.metersOnFeets(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.celsiusOnFahrenheit(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.litreOnGallon(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.centimetersOnInchs(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.kilogramOnPound(null));
    }
}
